activeCamera=scene.cameras.getByIndex(0);
function asyProjection() {activeCamera.projectionType=activeCamera.TYPE_PERSPECTIVE;
activeCamera.viewPlaneSize=0;
activeCamera.binding=activeCamera.BINDING_VERTICAL;
}

asyProjection();

handler=new CameraEventHandler();
runtime.addEventHandler(handler);
handler.onEvent=function(event) 
{
  asyProjection();
  scene.update();
}
var zero=new Vector3(0,0,0);
var nodes=scene.nodes;
var count=nodes.count;

var index=new Array();
for(i=0; i < count; i++) {
  var node=nodes.getByIndex(i); 
  var name=node.name;
  end=name.lastIndexOf(".")-1;
  if(end > 0) {
    if(name.substr(end,1) == "\001") {
      start=name.lastIndexOf("-")+1;
      n=end-start;
      if(n > 0) {
        index[name.substr(start,n)]=i;
        node.name=name.substr(0,start-1);
      }
    }
  }
}

var center=new Array(
Vector3(83.0816238896502,-7.12198159049176,-326.986008677774),
Vector3(-62.2237347897131,-37.4784977204793,-262.120226767226),
Vector3(-20.7126621185233,88.4418981447095,-325.282181911077),
Vector3(10.4289445499686,9.3191598229354,-279.755565268559),
);

billboardHandler=new RenderEventHandler();
billboardHandler.onEvent=function(event)
{
  var camera=scene.cameras.getByIndex(0); 
  var position=camera.position;
  var direction=position.subtract(camera.targetPosition);
  var up=camera.up.subtract(position);

  function f(i,k) {
    j=index[i];
    if(j >= 0) {
      var node=nodes.getByIndex(j);
      var name=node.name;
      var R=Matrix4x4();
      R.setView(zero,direction,up);
      var c=center[k];
      var T=node.transform;
      T.setIdentity();
      T.translateInPlace(c.scale(-1));
      T.multiplyInPlace(R);
      T.translateInPlace(c);
    }
  }
f(0,0);
f(1,1);
f(2,2);
f(3,3);

  runtime.refresh(); 
}
 
runtime.addEventHandler(billboardHandler); 

runtime.refresh(); 
